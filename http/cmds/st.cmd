# This should be a test startup script
require http,dev

epicsEnvSet("IOCNAME", "Simon's IOC")
epicsEnvSet("PORT1", "L0")
epicsEnvSet("PORT2", "L1")
epicsEnvSet("ADDR", "24")

drvHTTPPortConfigure($(PORT1), "localhost:80",0,0)
drvHTTPPortConfigure($(PORT2), "meas01.esss.lu.se:80",0,0)

# Here are the old asyn ones.
#drvAsynIPPortConfigure($(PORT1),"localhost:80 HTTP",0,0,0)
#drvAsynIPPortConfigure($(PORT2),"meas01.esss.lu.se:80 HTTP",0,0,0)

iocInit()

#asynSetTraceMask $(PORT) $(ADDR) 63
#asynSetTraceIOMask $(PORT) $(ADDR) 7

httppost L0 0 / test
