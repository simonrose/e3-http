The data that we need to send to the CABTR device in order to save a new file:

```
------------------------------9ac2a9cb444c
Content-Disposition: form-data; name="type"

upload
------------------------------9ac2a9cb444c
Content-Disposition: form-data; name="binary"; filename="$filename"
Content-Type: application/octet-stream

... file contents ...

------------------------------9ac2a9cb444c--
```
If we do this, then it works fine. So we need to add two sections to the POST output