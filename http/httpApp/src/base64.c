#include "base64.h"
#include <epicsTypes.h>
#include <string.h>

static const char ENCODED_CHARS[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void base64encode(const char *input, char *output)
{
    const size_t datalength = strlen(input);
    epicsUInt32 c;
    size_t i, j;

    for (i = 0, j = 0; i < datalength; i += 3, j += 4)
    {
        c = ((epicsUInt32)input[i]) << 16;
        if (i + 1 < datalength)
            c += ((epicsUInt32)input[i + 1]) << 8;
        if (i + 2 < datalength)
            c += ((epicsUInt32)input[i + 2]);

        output[j] = ENCODED_CHARS[c >> 18];
        output[j + 1] = ENCODED_CHARS[(c >> 12) & 0x3f];
        if (i + 1 < datalength)
            output[j + 2] = ENCODED_CHARS[(c >> 6) & 0x3f];
        else
            output[j + 2] = '=';
        if (i + 2 < datalength)
            output[j + 3] = ENCODED_CHARS[c & 0x3f];
        else
            output[j + 3] = '=';
    }
}

void base64decode(const char *input, char *output)
{
    const size_t datalength = strlen(input);
    epicsUInt32 c;
    size_t i, j;

    for (i = 0, j = 0; i < datalength; i += 4, j += 3)
    {
        c = ((epicsUInt32)(strchr(ENCODED_CHARS, input[i]) - ENCODED_CHARS)) << 18;
        c += ((epicsUInt32)(strchr(ENCODED_CHARS, input[i + 1]) - ENCODED_CHARS)) << 12;
        if (input[i + 2] != '=')
            c += ((epicsUInt32)(strchr(ENCODED_CHARS, input[i + 2]) - ENCODED_CHARS)) << 6;
        if (input[i + 3] != '=')
            c += ((epicsUInt32)(strchr(ENCODED_CHARS, input[i + 3]) - ENCODED_CHARS));

        output[j] = (char)(c >> 16);
        if (input[i + 2] != '=')
            output[j + 1] = (char)((c >> 8) & 0xff);
        if (input[i + 3] != '=')
            output[j + 2] = (char)(c & 0xff);
    }
}
