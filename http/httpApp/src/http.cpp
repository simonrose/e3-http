/* http.c */
/*
 *      Author: Simon Rose
 *      Date:   06MAY2020
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <vector>

#include <cantProceed.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsStdio.h>
#include <epicsString.h>
#include <epicsAssert.h>
#include <asynDriver.h>
#include <asynOctet.h>
#include <asynOctetSyncIO.h>
#include <drvAsynIPPort.h>
#include <iocsh.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <ellLib.h>

#include "base64.h"
#include "http.h"
#include "parser/parser.h"

#define BUFFER_SIZE 5121
#define HEADER_BUFFER_SIZE 640

static void *response_realloc(void *opaque, void *ptr, int size)
{
    return realloc(ptr, size);
}

static void response_body(void *opaque, const char *data, int size)
{
    /* We are ignoring the body */
}

static void response_header(void *opaque, const char *ckey, int nkey, const char *cvalue, int nvalue)
{
    /* We are ignoring the headers */
}

static void response_code(void *opaque, int code)
{
    *(int *)opaque = code;
}

static const http_funcs responseFuncs = {
    response_realloc,
    response_body,
    response_header,
    response_code,
};

typedef enum
{
    HTTP0_9,
    HTTP1_0,
    HTTP1_1,
    HTTP2,
    num_http_versions
} httpVersion;

/*
 * TODOs:
 * 
 * 1. Possibly merge the port/address/url into a data structure, akin to asynUser. Look at that a bit
 *    more to see if it is a reasonable comparison.
 */
ELLLIST httpPorts = {ELLNODE_INIT, 0};

typedef struct httpPort_t
{
    ELLNODE node;
    char *asynPort;
    char *hostInfo;
    char *hostName;
    int portNumber;
    int localPortNumber;
} httpPort_t;

static void cleanupHttpPort(httpPort_t *port)
{
    free(port->asynPort);
    free(port->hostInfo);
    free(port->hostName);
    free(port);
}

typedef enum
{
    GET,
    POST,
    num_http_methods
} httpMethods;

typedef struct httpMessage
{
    httpMethods method;
    char *url;
    httpVersion version;
    char *host;
    char *useragent;
    char *mimetype;
    char *header;
    char *xtrahdrs;
    char *msg;
    size_t msglen;
} httpMessage;

static void
cleanupHttpMessage(httpMessage *message)
{
    free(message->url);
    free(message->host);
    free(message->useragent);
    free(message->mimetype);
    free(message->header);
    free(message->xtrahdrs);
    free(message->msg);
    free(message);
}

static const char *methodstr(httpMethods method)
{
    switch (method)
    {
    case GET:
        return "GET";
    case POST:
        return "POST";
    default:
        return "Unsupported method";
    }
}

static const char *httpVersionStr(httpVersion version)
{
    switch (version)
    {
    case HTTP0_9:
        return "0.9";
    case HTTP1_0:
        return "1.0";
    case HTTP1_1:
        return "1.1";
    case HTTP2:
        return "2";
    default:
        return "Unsupported HTTP version";
    }
}

/*
 * Reads the user agent and IOC name from the environment variables, and writes them to <httpmess>.
 */
static void getUserAgent(httpMessage *httpmess)
{
    const int USER_AGENT_BUFFER_SIZE = 80;
    if (httpmess->useragent == NULL)
    {
        httpmess->useragent = (char *)callocMustSucceed(USER_AGENT_BUFFER_SIZE, sizeof(char), "getUserAgent()");
    }
    const char *ioc_name = getenv("IOCNAME");
    char *epics_base_version;

    char *c = strstr(getenv("EPICS_BASE"), "base-");
    if (c)
    {
        epics_base_version = epicsStrDup(c);
    }
    else
    {
        epics_base_version = NULL;
    }

    if (ioc_name)
    {
        sprintf(httpmess->useragent, "%s (EPICS %s IOC)", ioc_name, epics_base_version);
    }
    else
    {
        sprintf(httpmess->useragent, "EPICS %s IOC", epics_base_version);
    }
    free(epics_base_version);
}

/*
 * Builds the HTTP header. If a username/password is supplied, then it base64 encodes it and adds it in.
 * 
 * Note: Base64 encoding is reversible, so completely insecure. Do not send actual passwords this way.
 */
static asynStatus buildHttpHeader(char *buffer, const httpMessage *message, const char *username, const char *password)
{
    const char *header_fmt =
        "%s /%s HTTP/%s\r\n"
        "Host: %s\r\n"
        "User-Agent: %s\r\n"
        "Content-length: %d\r\n"
        "Connection: close\r\n"
        "%s\r\n";
    char *xtraheaders;
    if (username && password)
    {
        size_t passsize = (strlen(username) + strlen(password) + 1);
        size_t encsize = 4 * ((passsize + 2) / 3);
        char *passphrase = (char *)calloc(passsize + 1, sizeof(char));
        char *encodedpass = (char *)calloc(encsize + 1, sizeof(char));
        if (passphrase == NULL || encodedpass == NULL)
        {
            free(passphrase);
            free(encodedpass);
            return asynError;
        }

        xtraheaders = (char *)callocMustSucceed(strlen(message->xtrahdrs) + 101, sizeof(char), "buildHttpHeader");
        sprintf(passphrase, "%s:%s", username, password);
        base64encode(passphrase, encodedpass);
        sprintf(xtraheaders, "%sAuthorization: Basic %s\r\n", message->xtrahdrs, encodedpass);
        free(passphrase);
        free(encodedpass);
    }
    else
    {
        xtraheaders = epicsStrDup(message->xtrahdrs);
    }

    sprintf(buffer, header_fmt, methodstr(message->method), message->url, httpVersionStr(message->version), message->host, message->useragent, message->msglen, xtraheaders);
    free(xtraheaders);
    return asynSuccess;
}

static int randRange(int min, int max)
{
    return min + rand() / (RAND_MAX / (max - min) + 1);
}

/*
 * Generates a boundary string for separating multipart HTTP Post segments.
 */
static void buildBoundary(char *buffer, size_t boundarylen, const char *message)
{
    const char MULTIPART_CHARS[] = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const size_t l = strlen(MULTIPART_CHARS);
    size_t i;

    do
    {
        for (i = 0; i < boundarylen; i++)
            buffer[i] = MULTIPART_CHARS[randRange(0, l)];
    } while (strstr(message, buffer)); // unlikely, but make sure that the boundary does not occur in the message.
}

static httpPort_t *findHTTPPort(const char *port)
{
    httpPort_t *pport = (httpPort_t *)ellFirst(&httpPorts);
    while (pport)
    {
        if (strcmp(pport->asynPort, port) == 0)
            break;
        pport = (httpPort_t *)ellNext(&pport->node);
    }
    return pport;
}

epicsShareFunc unsigned int httpGetResponseCode(const char *response)
{
    http_roundtripper rt;
    int http_response;
    http_init(&rt, responseFuncs, &http_response);

    bool needmore = true;
    size_t length = strlen(response);
    while (needmore && length)
    {
        int read;
        needmore = http_data(&rt, response, length, &read);
        length -= read;
        response += read;
    }
    http_free(&rt);

    return http_response;
}

/*
 * Public function to post data via the port <port> configured in <drvHTTPPortConfigure>.
 * 
 * <url>: URL to post to.  Local to the host specified in <port>.
 * <xtrahdrs>: Extra headers to pass along, depending on the message.
 * <msg>: Body of message to send to remote host.
 */
epicsShareFunc int httpPostData(const char *port, int addr, const char *url, const char *xtrahdrs, const char *msg, char *response, int maxchars)
{
    asynUser *pasynUser;
    asynStatus status;
    httpMessage *httpmess;
    httpPort_t *pport;

    char header_buffer[HEADER_BUFFER_SIZE];
    size_t nbytesout = 0, nbytesin = 0;
    int eomReason;

    if (port == NULL)
    {
        return -1;
    }
    if (url == NULL)
    {
        return -1;
    }
    if (msg == NULL)
    {
        return -1;
    }
    pport = findHTTPPort(port);
    if (pport == NULL)
    {
        return -1;
    }

    status = pasynOctetSyncIO->connect(port, addr, &pasynUser, NULL);
    if (status != asynSuccess)
    {
        printf("can't connect to port %s\n", pasynUser->errorMessage);
        return -1;
    }

    httpmess = (httpMessage *)callocMustSucceed(1, sizeof(httpMessage), "httpMessageSync");
    httpmess->method = POST;
    httpmess->host = epicsStrDup(pport->hostName);
    httpmess->version = HTTP1_1;
    httpmess->xtrahdrs = epicsStrDup(xtrahdrs);
    httpmess->msg = epicsStrDup(msg);
    httpmess->msglen = strlen(msg);
    httpmess->url = epicsStrDup(url);
    getUserAgent(httpmess);

    status = buildHttpHeader(header_buffer, httpmess, "simonrose", "test");
    if (status != asynSuccess)
    {
        printf("buildHttpHeader failed.\n");
        cleanupHttpMessage(httpmess);
        return -1;
    }

    status = pasynOctetSyncIO->write(pasynUser, header_buffer, strlen(header_buffer), 1.0, &nbytesout);
    if (status != asynSuccess || nbytesout != strlen(header_buffer))
    {
        printf("httpPostData write header failed.\n");
        cleanupHttpMessage(httpmess);
        return -1;
    }

    status = pasynOctetSyncIO->write(pasynUser, httpmess->msg, httpmess->msglen, 1.0, &nbytesout);
    if (status != asynSuccess || nbytesout != httpmess->msglen)
    {
        printf("httpPostData write message failed.\n");
        cleanupHttpMessage(httpmess);
        return -1;
    }
    /*
     * TODO: There seems to be a problem here if we have a response that is longer than the
     * "max response size". In that case, the next call to httpPostData may fail.
     * 
     * The odd thing is, it only seems to fail when it tries to write the /message/, not the
     * header, so it isn't failing at the first write when there is something left in the
     * read buffer.
     * 
     * Note that the second call may give an error, and it may not. But it sometimes will
     * only return the rest of the message that we had not received yet, so it seems that we
     * need to call read() over and over until there is no message left, which is probably 
     * pretty reasonable.
     */
    status = pasynOctetSyncIO->read(pasynUser, response, maxchars, 1.0, &nbytesin, &eomReason);
    printf("----------------------------------------------\n"
           "pasynOctet->read(): nbytesin  = %d\n"
           "                    status    = %d\n"
           "                    eomReason = %d\n"
           "                    lastchar  = %d\n"
           "----------------------------------------------\n",
           nbytesin, status, eomReason, response[nbytesin]);
    if (status != asynSuccess)
    {
        printf("httpPostRead call to pasynOctet->read() failed: Status %d\n", status);
        cleanupHttpMessage(httpmess);
        return -1;
    }
    if (eomReason & ASYN_EOM_CNT)
    {
        status = pasynOctetSyncIO->flush(pasynUser);
        if (status != asynSuccess)
        {
            printf("pasynOctetSyncIO->flush() failed: Status %d\n", status);
        }
    }

    status = pasynOctetSyncIO->disconnect(pasynUser);

    cleanupHttpMessage(httpmess);
    return status;
}

/*
 * Sends a file via HTTP post.
 * 
 * <name>:     Variable name that the server is looking for.
 * <filename>: Filename.
 * <content>:  Content of file to send.
 */
epicsShareFunc int httpSendFile(const char *port, int addr, const char *url, httpVar_t filename, const char *content, char *response, int maxchars)
{
    int status;
    const size_t BOUNDARY_SIZE = 30;
    char boundary[BOUNDARY_SIZE];
    const char *message_fmt =
        "--%s\r\n"
        "Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n"
        "Content-Type: application/x-object\r\n\r\n"
        "%s\r\n"
        "--%s--\r\n";
    const char *hdr_fmt = "Content-type: multipart/form-data; boundary=%s\r\n";
    char *msg;
    char *xtrahdr;
    buildBoundary(boundary, BOUNDARY_SIZE, content);

    //msg = callocMustSucceed(BUFFER_SIZE, sizeof(char), "httpSendFile");
    status = asprintf(&msg, message_fmt, boundary, filename.varname, filename.value, content, boundary);
    if (status < 0)
    {
        return status;
    }

    //xtrahdr = callocMustSucceed(strlen(hdr_fmt) + BOUNDARY_SIZE + 1, sizeof(char), "httpSendFile");
    status = asprintf(&xtrahdr, hdr_fmt, boundary);
    if (status < 0)
        return status;
    status = httpPostData(port, addr, url, xtrahdr, msg, response, maxchars);
    free(msg);
    free(xtrahdr);
    return status;
}

/*
 * Wrapper function for <drvAsynIPPortConfigure>. We need to keep the hostname, which
 * is kept in a private data structure in drvAsynIPPort.c
 */
static long drvHTTPPortConfigure(const char *portName, const char *hostInfo, unsigned int priority, int noAutoConnect)
{
    const char *functionName = "drvHTTPPortConfigure";
    httpPort_t *pport;
    char *asynHostInfo;
    char *cp;
    int status;

    pport = (httpPort_t *)callocMustSucceed(1, sizeof(httpPort_t), "drvHTTPPortConfigure failure");
    pport->asynPort = epicsStrDup(portName);
    pport->hostInfo = epicsStrDup(hostInfo);
    if ((cp = strchr(pport->hostInfo, ':')) == NULL)
    {
        printf("%s: \"%s\" is not of the form \"<host>:<port>[:localPort]\"\n",
               functionName, hostInfo);
        cleanupHttpPort(pport);
        return -1;
    }
    *cp = '\0';
    pport->hostName = epicsStrDup(pport->hostInfo);
    *cp = ':';
    if (sscanf(cp, ":%d", &pport->portNumber) < 1)
    {
        printf("%s: \"%s\" is not of the form \"<host>:<port>[:localPort]\"\n",
               functionName, hostInfo);
        cleanupHttpPort(pport);
        return -1;
    }

    asynHostInfo = (char *)callocMustSucceed(strlen(hostInfo) + 6, sizeof(char), "drvHTTPPortConfigure failure");
    sprintf(asynHostInfo, "%s HTTP", hostInfo);
    status = drvAsynIPPortConfigure(portName, asynHostInfo, priority, noAutoConnect, 0);
    free(asynHostInfo);
    if (status)
    {
        printf("drvHTTPPortConfigure::drvAsynIPPortConfigure failed.\n");
        cleanupHttpPort(pport);
        return status;
    }
    ellAdd(&httpPorts, &pport->node);
    return 0;
}

static const iocshArg drvHTTPPortConfigureArg0 = {"port name", iocshArgString};
static const iocshArg drvHTTPPortConfigureArg1 = {"host:port", iocshArgString};
static const iocshArg drvHTTPPortConfigureArg2 = {"priority", iocshArgInt};
static const iocshArg drvHTTPPortConfigureArg3 = {"disable auto-connect", iocshArgInt};
static const iocshArg *const drvHTTPPortConfigureArgs[] = {&drvHTTPPortConfigureArg0, &drvHTTPPortConfigureArg1, &drvHTTPPortConfigureArg2, &drvHTTPPortConfigureArg3};
static const iocshFuncDef drvHTTPPortConfigureDef = {"drvHTTPPortConfigure", 4, drvHTTPPortConfigureArgs};
static void drvHTTPPortConfigureCall(const iocshArgBuf *args)
{
    drvHTTPPortConfigure(args[0].sval, args[1].sval, args[2].ival, args[3].ival);
}
static void drvHTTPPortConfigureRegister(void)
{
    static int firstTime = 1;
    if (!firstTime)
        return;
    firstTime = 0;
    iocshRegister(&drvHTTPPortConfigureDef, drvHTTPPortConfigureCall);
}
epicsExportRegistrar(drvHTTPPortConfigureRegister);

static const iocshArg httppostArg0 = {"port", iocshArgString};
static const iocshArg httppostArg1 = {"address", iocshArgInt};
static const iocshArg httppostArg2 = {"page", iocshArgString};
static const iocshArg httppostArg3 = {"message", iocshArgString};
static const iocshArg *const httppostArgs[] = {&httppostArg0, &httppostArg1, &httppostArg2, &httppostArg3};
static const iocshFuncDef httppostDef = {"httppost", 4, httppostArgs};
static void httppostCall(const iocshArgBuf *args)
{
    //httpPostSync(args[0].sval, args[1].ival, args[2].sval);
    //httpMessageSync(args[0].sval, args[1].ival, args[2].sval, args[3].sval, POST);
    httpVar_t filename = {"filename", "outfile.txt"};
    char *buffer = (char *)calloc(BUFFER_SIZE, sizeof(char));
    httpSendFile(args[0].sval, args[1].ival, args[2].sval, filename, args[3].sval, buffer, BUFFER_SIZE - 1);
    printf("===== output ======\n");
    printf(buffer);
    free(buffer);
}

static void httppostRegister(void)
{
    static int firstTime = 1;
    if (!firstTime)
        return;
    firstTime = 0;
    iocshRegister(&httppostDef, httppostCall);
}

epicsExportRegistrar(httppostRegister);
