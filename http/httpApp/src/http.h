
#ifndef INC_HTTP_H
#define INC_HTTP_H

#include <shareLib.h>

typedef struct httpVar_t
{
    const char *varname;
    const char *value;
} httpVar_t;

epicsShareFunc int httpPostData(const char *port, int addr, const char *url, const char *xtrahdrs, const char *msg, char *response, int maxchars);
#ifdef __cplusplus
extern "C"
{
#endif
    epicsShareFunc unsigned int httpGetResponseCode(const char *response);
    epicsShareFunc int httpSendFile(const char *port, int addr, const char *url, httpVar_t filename, const char *content, char *response, int maxchars);
#ifdef __cplusplus
}
#endif

#endif