#ifndef INC_base64_H
#define INC_base64_H

#include <stdlib.h>
#ifdef __cplusplus
extern "C"
{
#endif
    void base64encode(const char *input, char *output);
    void base64decode(const char *input, char *output);
#ifdef __cplusplus
};
#endif
#endif